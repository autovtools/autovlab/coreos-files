#!/bin/bash
export REGISTRY=$(cat registry.url)
for SVC in $(find . -name "build.sh"); do
    pushd "$(dirname "${SVC}")"
    ./build.sh && ./push.sh
    echo "####################################################"
    popd
done
