#!/bin/bash

if [ "$1" = "" ]; then
    echo "USAGE: $0 /dir/to/expose "
    echo "Exposes a directory (recursively) to containers by "
    echo "   changing the SELinux context to svirt_sandbox_file_t"
    echo "Only use on directories that need to be bind-mounted inside containers!!!"
    exit 1
fi

echo "!!!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!!!"
echo "! This will allow containers to bind-mount:"
echo "!     '${1}' ('$(readlink -f "${1}")')"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "Press any key to continue, or CTRL+C to cancel"
read -s -n 1
sudo chcon -Rt svirt_sandbox_file_t "${1}"
