if [ "$1" == "" ]; then
    echo "Usage: ${0} SVC"
    exit 1
fi
SVC="${1}"
pushd "/opt/swarm/${SVC}"
    docker service rm "${SVC}_${SVC}"
    if [ -f stack.env ]; then
	export $(cat stack.env | xargs)
    fi
    docker stack deploy -c stack.yml "${SVC}"
popd
