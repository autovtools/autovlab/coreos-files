#!/bin/bash

docker service rm $(docker service ls | awk '{print $2}' | tail -n +2)
docker secret rm $(docker secret ls | awk '{print $2}' | tail -n +2)
docker config rm $(docker config ls | awk '{print $2}' | tail -n +2)
