#!/bin/bash

docker swarm leave --force
sleep 1
docker volume rm $(docker volume ls -q)
docker swarm init

HOST="$(hostname)"
for SVC in autostepca autocertman registry registry-mirror; do 
    docker node update --label-add ${SVC}=enabled "${HOST}"
done
